import { DataTypes, Model, Optional, Sequelize } from "sequelize";
import { Models } from "../interfaces/general";

interface ExperienceAttributes {
  id: number;
  userId: number;
  companyName: string;
  role: string;
  startDate: Date;
  endDate: Date;
  description: string;
}

export class Experience
  extends Model<
    ExperienceAttributes,
    Optional<ExperienceAttributes, "id" | "endDate">
  >
  implements ExperienceAttributes
{
  id: number;
  userId: number;
  companyName: string;
  role: string;
  startDate: Date;
  endDate: Date;
  description: string;

  readonly createdAt: Date;
  readonly updatedAt: Date;

  static defineSchema(sequelize: Sequelize) {
    Experience.init(
      {
        id: {
          type: DataTypes.INTEGER.UNSIGNED,
          primaryKey: true,
          autoIncrement: true,
        },
        userId: {
          type: DataTypes.INTEGER.UNSIGNED,
          references: {
            model: {
              tableName: "experiences",
            },
            key: "id",
          },
          allowNull: false,
        },
        companyName: {
          type: new DataTypes.STRING(256),
          allowNull: false,
        },
        role: {
          type: new DataTypes.STRING(256),
          allowNull: false,
        },
        startDate: {
          type: DataTypes.DATE,
          allowNull: false,
        },
        endDate: DataTypes.DATE,
        description: {
          type: DataTypes.TEXT,
          allowNull: false,
        },
      },
      { tableName: "experiences", underscored: true, sequelize }
    );
  }

  static associate(models: Models) {
    Experience.belongsTo(models.user, {
      foreignKey: "user_id",
    });
  }
}
