import supertest from "supertest";
import { loadApp } from "../loaders/app";
import { cacheService } from "../services/cache.service";
import path from "path";

let request: ReturnType<typeof supertest>;

let adminToken: string;

beforeAll(async () => {
  const app = await loadApp();

  request = supertest(app);

  const res = await request
    .post("/api/auth/login")
    .field("email", "admin1@admin.com")
    .field("password", "AdminPassword");

  adminToken = res.body.token;
});

afterAll(async () => {
  // server.close();

  cacheService.quit();
});

describe("POST /users", () => {
  const imagePath = path.join(__dirname, "../../public/default.png");

  it("should return 400 when email is already in use", async () => {
    const res = await request
      .post("/api/users")
      .field("email", "admin1@admin.com")
      .field("password", "testpassword")
      .field("firstName", "Test")
      .field("lastName", "test")
      .field("title", "title")
      .field("summary", "summary")
      .attach("image", imagePath)
      .set("Authorization", "Bearer " + adminToken);

    expect(res.statusCode).toEqual(400);
  });

  it("should return 201 when the input is correct and the request is from an admin", async () => {
    const res = await request
      .post("/api/users")
      .field("email", "test1@test.com")
      .field("password", "testpassword")
      .field("firstName", "Test")
      .field("lastName", "test")
      .field("title", "title")
      .field("summary", "summary")
      .field("role", "User")
      .attach("image", imagePath)
      .set("Authorization", "Bearer " + adminToken);

    expect(res.statusCode).toEqual(201);
  });
});

describe("GET /users", () => {
  it("should return 200 when the request is from a admin", async () => {
    const res = await request
      .get("/api/users")
      .set("Authorization", "Bearer " + adminToken)
      .query({ pageSize: 2, page: 1 });

    expect(res.statusCode).toEqual(200);
  });
});

describe("GET /:id", () => {
  it("should return 400 if the id is invalid", async () => {
    const res = await request.get("/api/users/-5");

    expect(res.statusCode).toEqual(400);
  });

  it("should return 404 if the user cannot be found", async () => {
    const res = await request.get("/api/users/500");

    expect(res.statusCode).toEqual(404);
  });

  it("should return 200 if the user exists", async () => {
    const res = await request.get("/api/users/1");

    expect(res.statusCode).toEqual(200);
  });
});

describe("PUT /:id", () => {
  it("should return 400 when fields are not attached", async () => {
    const res = await request
      .put("/api/users/1")
      .set("Authorization", "Bearer " + adminToken);

    expect(res.statusCode).toEqual(400);
  });

  it("should return 200 when everything is in order", async () => {
    const res = await request
      .put("/api/users/1")
      .field("email", "admin1@admin.com")
      .field("password", "AdminPassword")
      .field("role", "Admin")
      .field("summary", "summary")
      .field("title", "title")
      .field("firstName", "Admin")
      .field("lastName", "Admin")
      .set("Authorization", "Bearer " + adminToken);

    expect(res.statusCode).toEqual(200);
  });

  it("should return 403 when id does not match the requesting user", async () => {
    const res = await request
      .put("/api/users/500")
      .field("email", "admin1@admin.com")
      .field("password", "AdminPassword")
      .field("role", "Admin")
      .field("summary", "summary")
      .field("title", "title")
      .field("firstName", "Admin")
      .field("lastName", "Admin")
      .set("Authorization", "Bearer " + adminToken);

    expect(res.statusCode).toEqual(403);
  });
});

describe("DELETE /:id", () => {
  let userToken: string;
  let userId: string;

  beforeAll(async () => {
    const res = await request
      .post("/api/auth/login")
      .field("email", "test1@test.com")
      .field("password", "testpassword");

    userToken = res.body.token;
    userId = res.body.user.id;
  });

  it("should return 404 if the user does not exist", async () => {
    const res = await request
      .delete("/api/users/500")
      .set("Authorization", "Bearer " + adminToken);

    expect(res.statusCode).toEqual(404);
  });

  it("should return 403 if a normal user is trying to delete somebody elses account", async () => {
    const res = await request
      .delete("/api/users/1")
      .set("Authorization", "Bearer " + userToken);

    expect(res.statusCode).toEqual(403);
  });

  it("should return 204 if the user was successfully deleted", async () => {
    const res = await request
      .delete("/api/users/" + userId)
      .set("Authorization", "Bearer " + userToken);

    expect(res.statusCode).toEqual(204);
  });
});
