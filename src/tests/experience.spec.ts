import supertest from "supertest";
import { loadApp } from "../loaders/app";
import { cacheService } from "../services/cache.service";
import path from "path";

let request: ReturnType<typeof supertest>;

let adminToken: string;
let userId: string;
let userToken: string;

beforeAll(async () => {
  const app = await loadApp();

  request = supertest(app);

  const res = await request
    .post("/api/auth/login")
    .field("email", "admin1@admin.com")
    .field("password", "AdminPassword");

  adminToken = res.body.token;
});

afterAll(async () => {
  // server.close();

  cacheService.quit();
});

describe("POST /experience", () => {
  const imagePath = path.join(__dirname, "../../public/default.png");

  beforeAll(async () => {
    await request
      .post("/api/auth/register")
      .field("email", "test5@test.com")
      .field("password", "testpassword")
      .field("firstName", "Test")
      .field("lastName", "test")
      .field("title", "title")
      .field("summary", "summary")
      .attach("image", imagePath);
  });

  it("should return 400 when fields are missing", async () => {
    const res = await request
      .post("/api/experience")
      .set("Authorization", "Bearer " + adminToken);

    expect(res.statusCode).toEqual(400);
  });

  it("should return 403 if the ids don't match", async () => {
    const res = await request
      .post("/api/experience")
      .set("Authorization", "Bearer " + adminToken)
      .field("id", 5)
      .field("companyName", "XYZ COmpany")
      .field("role", "SOME role")
      .field("startDate", "2012-05-05")
      .field("endDate", "2012-05-05")
      .field("description", "some description");

    expect(res.statusCode).toEqual(403);
  });

  it("should return 201 if the experience was created", async () => {
    const res = await request
      .post("/api/experience")
      .set("Authorization", "Bearer " + adminToken)
      .field("userId", "1")
      .field("companyName", "XYZ COmpany")
      .field("role", "SOME role")
      .field("startDate", "2012-05-05")
      .field("endDate", "2013-05-05")
      .field("description", "some description");

    expect(res.statusCode).toEqual(201);
  });
});

describe("GET /experience", () => {
  beforeAll(async () => {
    const res = await request
      .post("/api/auth/login")
      .field("email", "test5@test.com")
      .field("password", "testpassword");

    userId = res.body.user.id;
    userToken = res.body.token;
  });

  it("should return 403 when a normal user is sending a request", async () => {
    const res = await request
      .get("/api/experience")
      .set("Authorization", "Bearer " + userToken)
      .query({ pageSize: 2, page: 1 });

    expect(res.statusCode).toEqual(403);
  });

  it("should return 400 when query parameters are missing", async () => {
    const res = await request
      .get("/api/experience")
      .set("Authorization", "Bearer " + adminToken);

    expect(res.statusCode).toEqual(400);
  });

  it("should return 200 when everything is in order", async () => {
    const res = await request
      .get("/api/experience")
      .set("Authorization", "Bearer " + adminToken)
      .query({
        pageSize: 2,
        page: 1,
      });

    expect(res.statusCode).toEqual(200);
  });
});

describe("GET /:id", () => {
  it("should return 404 if experience does not exist", async () => {
    const res = await request.get("/api/experience/500");

    expect(res.statusCode).toEqual(404);
  });

  it("should return 400 if id is invalid", async () => {
    const res = await request.get("/api/experience/1.23");

    expect(res.statusCode).toEqual(400);
  });

  it("should return 200 if experience exists", async () => {
    const res = await request.get("/api/experience/1");

    expect(res.statusCode).toEqual(200);
  });
});

describe("PUT /:id", () => {
  it("should return 404 if experience does not exist", async () => {
    const res = await request
      .put("/api/experience/500")
      .set("Authorization", "Bearer " + adminToken)
      .field("userId", "300")
      .field("companyName", "XYZ COmpany")
      .field("role", "SOME role")
      .field("startDate", "2012-05-05")
      .field("endDate", "2013-05-05")
      .field("description", "some description");

    expect(res.statusCode).toEqual(404);
  });

  it("should return 400 if user ids do not match", async () => {
    const res = await request
      .put("/api/experience/1")
      .set("Authorization", "Bearer " + adminToken)
      .field("userId", "300")
      .field("companyName", "XYZ COmpany")
      .field("role", "SOME role")
      .field("startDate", "2012-05-05")
      .field("endDate", "2013-05-05")
      .field("description", "some description");

    expect(res.statusCode).toEqual(400);
  });

  it("should return 403 if a user is trying to edit another user's experience", async () => {
    const res = await request
      .put("/api/experience/1")
      .set("Authorization", "Bearer " + userToken)
      .field("userId", "1")
      .field("companyName", "XYZ COmpany")
      .field("role", "SOME role")
      .field("startDate", "2012-05-05")
      .field("endDate", "2013-05-05")
      .field("description", "some description");

    expect(res.statusCode).toEqual(403);
  });

  it("should return 200 if everything is in order", async () => {
    const res = await request
      .put("/api/experience/1")
      .set("Authorization", "Bearer " + adminToken)
      .field("userId", "1")
      .field("companyName", "XYZ COmpany")
      .field("role", "SOME role")
      .field("startDate", "2012-05-05")
      .field("endDate", "2013-05-05")
      .field("description", "some description");

    expect(res.statusCode).toEqual(200);
  });
});

describe("DELETE /:id", () => {
  let expId: string;

  beforeAll(async () => {
    const res = await request
      .post("/api/experience")
      .set("Authorization", "Bearer " + userToken)
      .field("userId", userId)
      .field("companyName", "XYZ COmpany")
      .field("role", "SOME role")
      .field("startDate", "2012-05-05")
      .field("endDate", "2013-05-05")
      .field("description", "some description");

    expId = res.body.id;
  });

  it("should return 400 if id is invalid", async () => {
    const res = await request
      .delete("/api/experience/1.23")
      .set("Authorization", "Bearer " + adminToken);

    expect(res.statusCode).toEqual(400);
  });

  it("should return 404 if experience cannot be found", async () => {
    const res = await request
      .delete("/api/experience/500")
      .set("Authorization", "Bearer " + adminToken);

    expect(res.statusCode).toEqual(404);
  });

  it("should return 403 if a user is trying to delete another user's experience", async () => {
    const res = await request
      .delete("/api/experience/1")
      .set("Authorization", "Bearer " + userToken);

    expect(res.statusCode).toEqual(403);
  });

  it("should return 204 when an admin deletes another user's experience", async () => {
    const res = await request
      .delete("/api/experience/" + expId)
      .set("Authorization", "Bearer " + adminToken);

    expect(res.statusCode).toEqual(204);
  });

  it("should return 204 when a user deletes their own experience", async () => {
    const res = await request
      .delete("/api/experience/1")
      .set("Authorization", "Bearer " + adminToken);

    expect(res.status).toEqual(204);
  });
});
