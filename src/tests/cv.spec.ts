import supertest from "supertest";
import { loadApp } from "../loaders/app";
import { cacheService } from "../services/cache.service";

let request: ReturnType<typeof supertest>;

beforeAll(async () => {
  const app = await loadApp();

  request = supertest(app);
});

afterAll(async () => {
  // server.close();

  cacheService.quit();
});

describe("GET /cv", () => {
  it("should return 400 if id is invalid", async () => {
    const res = await request.get("/api/user/2.32/cv");

    expect(res.statusCode).toEqual(400);
  });

  it("should return 404 if user does not exist", async () => {
    const res = await request.get("/api/user/500/cv");

    expect(res.statusCode).toEqual(404);
  });

  it("should return 200 if everything is in order", async () => {
    const res = await request.get("/api/user/1/cv");

    expect(res.statusCode).toEqual(200);
  });

  it("should fetch feedbacks, projects and experiences when a user exists", async () => {
    const res = await request.get("/api/user/1/cv");

    expect(res.body).toHaveProperty("Projects");
    expect(res.body).toHaveProperty("Feedbacks");
    expect(res.body).toHaveProperty("Experiences");
  });
});
