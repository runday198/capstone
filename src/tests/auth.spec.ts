import supertest from "supertest";
import { loadApp } from "../loaders/app";
import { cacheService } from "../services/cache.service";
import path from "path";

let request: ReturnType<typeof supertest>;

beforeAll(async () => {
  const app = await loadApp();
  request = supertest(app);
});

afterAll(async () => {
  // server.close();

  cacheService.quit();
});

describe("POST /register", () => {
  const imagePath = path.join(__dirname, "../../public/default.png");

  it("should return 400 when image is missing", async () => {
    const res = await request.post("/api/auth/register").send({
      email: "test@test.com",
      password: "testpassword",
      firstName: "Test",
      lastName: "Test",
      title: "Title",
      summary: "Summary",
    });

    expect(res.statusCode).toEqual(400);
  });

  it("should return 400 when password is too short", async () => {
    const res = await request
      .post("/api/auth/register")
      .field("email", "test@test.com")
      .field("password", "ts")
      .field("firstName", "Test")
      .field("lastName", "test")
      .field("title", "title")
      .field("summary", "summary")
      .attach("image", imagePath);

    expect(res.statusCode).toEqual(400);
  });

  it("should return 201 when everything is correct", async () => {
    const res = await request
      .post("/api/auth/register")
      .field("email", "test@test.com")
      .field("password", "testpassword")
      .field("firstName", "Test")
      .field("lastName", "test")
      .field("title", "title")
      .field("summary", "summary")
      .attach("image", imagePath);

    expect(res.statusCode).toEqual(201);
  });

  it("should return 400 when email is already in use", async () => {
    const res = await request
      .post("/api/auth/register")
      .field("email", "test@test.com")
      .field("password", "testpassword")
      .field("firstName", "Test1")
      .field("lastName", "test1")
      .field("title", "title")
      .field("summary", "summary")
      .attach("image", imagePath);

    expect(res.statusCode).toEqual(400);
  });
});

describe("POST /login", () => {
  it("should return 401 when email or password are incorrect", async () => {
    const res = await request
      .post("/api/auth/login")
      .field("email", "test@test.com")
      .field("password", "incorrectPassword");

    expect(res.statusCode).toEqual(401);
  });

  it("should return 200 when credentials are correct", async () => {
    const res = await request
      .post("/api/auth/login")
      .field("email", "test@test.com")
      .field("password", "testpassword");

    expect(res.statusCode).toEqual(200);
  });
});
