import supertest from "supertest";
import { loadApp } from "../loaders/app";
import { cacheService } from "../services/cache.service";
import path from "path";

let request: ReturnType<typeof supertest>;

let adminToken: string;
let userId: string;
let userToken: string;

beforeAll(async () => {
  const app = await loadApp();

  request = supertest(app);

  const res = await request
    .post("/api/auth/login")
    .field("email", "admin1@admin.com")
    .field("password", "AdminPassword");

  adminToken = res.body.token;
});

afterAll(async () => {
  // server.close();

  cacheService.quit();
});

describe("POST /project", () => {
  const imagePath = path.join(__dirname, "../../public/default.png");

  beforeAll(async () => {
    await request
      .post("/api/auth/register")
      .field("email", "test10@test.com")
      .field("password", "testpassword")
      .field("firstName", "Test")
      .field("lastName", "test")
      .field("title", "title")
      .field("summary", "summary")
      .attach("image", imagePath);
  });

  it("should return 400 if fields are missing", async () => {
    const res = await request
      .post("/api/project")
      .set("Authorization", "Bearer " + adminToken);

    expect(res.statusCode).toEqual(400);
  });

  it("should return 403 if ids dont match", async () => {
    const res = await request
      .post("/api/project")
      .set("Authorization", "Bearer " + adminToken)
      .field("userId", "500")
      .field("description", "SOme description")
      .attach("image", imagePath);

    console.log(res.body);
    expect(res.statusCode).toEqual(403);
  });

  it("should return 201 when everything is in order", async () => {
    const res = await request
      .post("/api/project")
      .set("Authorization", "Bearer " + adminToken)
      .field("userId", "1")
      .field("description", "SOme description")
      .attach("image", imagePath);

    expect(res.statusCode).toEqual(201);
  });
});

describe("GET /project", () => {
  beforeAll(async () => {
    const res = await request
      .post("/api/auth/login")
      .field("email", "test10@test.com")
      .field("password", "testpassword");

    userToken = res.body.token;
    userId = res.body.user.id;
  });

  it("should return 403 if a normal user sends a request", async () => {
    const res = await request
      .get("/api/project")
      .set("Authorization", "Bearer " + userToken);

    expect(res.statusCode).toEqual(403);
  });

  it("should return 400 if query parameters are missing", async () => {
    const res = await request
      .get("/api/project")
      .set("Authorization", "Bearer " + adminToken);

    expect(res.statusCode).toEqual(400);
  });

  it("should return 200 if everything is in order", async () => {
    const res = await request
      .get("/api/project")
      .set("Authorization", "Bearer " + adminToken)
      .query({
        pageSize: 2,
        page: 1,
      });

    expect(res.statusCode).toEqual(200);
  });
});

describe("GET /:id", () => {
  it("should return 400 if id is invalid", async () => {
    const res = await request.get("/api/project/1.23");

    expect(res.statusCode).toEqual(400);
  });

  it("should return 404 if the project does not exist", async () => {
    const res = await request.get("/api/project/500");

    expect(res.statusCode).toEqual(404);
  });

  it("should return 200 if everything is in order", async () => {
    const res = await request.get("/api/project/1");

    expect(res.statusCode).toEqual(200);
  });
});

describe("PUT /:id", () => {
  it("should return 400 if id is invalid", async () => {
    const res = await request
      .put("/api/project/1.23")
      .set("Authorization", "Bearer " + adminToken);

    expect(res.statusCode).toEqual(400);
  });

  it("should return 403 if a user is trying to edit another user's project", async () => {
    const res = await request
      .put("/api/project/1")
      .set("Authorization", "Bearer " + adminToken)
      .field("userId", "500")
      .field("description", "description");

    expect(res.statusCode).toEqual(403);
  });

  it("should return 404 if the project does not exist", async () => {
    const res = await request
      .put("/api/project/500")
      .set("Authorization", "Bearer " + adminToken)
      .field("userId", "1")
      .field("description", "some description");

    expect(res.statusCode).toEqual(404);
  });

  it("should return 403 if ids dont match", async () => {
    const res = await request
      .put("/api/project/1")
      .set("Authorization", "Bearer " + userToken)
      .field("userId", userId)
      .field("description", "someDescirption");

    expect(res.statusCode).toEqual(403);
  });

  it("should return 200 if everything is in order", async () => {
    const res = await request
      .put("/api/project/1")
      .set("Authorization", "Bearer " + adminToken)
      .field("userId", "1")
      .field("description", "SOmedescription");

    expect(res.statusCode).toEqual(200);
  });
});

describe("DETELE /:id", () => {
  const imagePath = path.join(__dirname, "../../public/default.png");
  let projectId: string;
  beforeAll(async () => {
    const res = await request
      .post("/api/project")
      .set("Authorization", "Bearer " + userToken)
      .field("userId", userId)
      .field("description", "description")
      .attach("image", imagePath);

    projectId = res.body.id;
  });

  it("should return 400 if id is invalid", async () => {
    const res = await request
      .delete("/api/project/12.3")
      .set("Authorization", "Bearer " + adminToken);

    expect(res.statusCode).toEqual(400);
  });

  it("should return 404 if project does not exist", async () => {
    const res = await request
      .delete("/api/project/500")
      .set("Authorization", "Bearer " + adminToken);

    expect(res.statusCode).toEqual(404);
  });

  it("should return 403 if a user is trying to delete another user's project", async () => {
    const res = await request
      .delete("/api/project/1")
      .set("Authorization", "Bearer " + userToken);

    expect(res.statusCode).toEqual(403);
  });

  it("should return 204 if an admin deletes another user's project", async () => {
    const res = await request
      .delete("/api/project/" + projectId)
      .set("Authorization", "Bearer " + adminToken);

    expect(res.statusCode).toEqual(204);
  });

  it("should return 204 if a user is trying to delete their own project", async () => {
    const res = await request
      .delete("/api/project/1")
      .set("Authorization", "Bearer " + adminToken);

    expect(res.statusCode).toEqual(204);
  });
});
