import supertest from "supertest";
import { loadApp } from "../loaders/app";
import { cacheService } from "../services/cache.service";
import path from "path";

let request: ReturnType<typeof supertest>;

let adminToken: string;
let userId: string;
let userToken: string;

beforeAll(async () => {
  const app = await loadApp();

  request = supertest(app);

  const res = await request
    .post("/api/auth/login")
    .field("email", "admin1@admin.com")
    .field("password", "AdminPassword");

  adminToken = res.body.token;
});

afterAll(async () => {
  // server.close();

  cacheService.quit();
});

describe("POST /feedback", () => {
  const imagePath = path.join(__dirname, "../../public/default.png");
  beforeAll(async () => {
    const res = await request
      .post("/api/auth/register")
      .field("email", "test15@test.com")
      .field("password", "testpassword")
      .field("firstName", "Test")
      .field("lastName", "test")
      .field("title", "title")
      .field("summary", "summary")
      .attach("image", imagePath);
    userId = res.body.id;
  });

  it("should return 400 if a field is missing", async () => {
    const res = await request
      .post("/api/feedback")
      .set("Authorization", "Bearer " + adminToken);

    expect(res.statusCode).toEqual(400);
  });

  it("should return 403 if a user is trying to create another user's feedback", async () => {
    const res = await request
      .post("/api/feedback")
      .set("Authorization", "Bearer " + adminToken)
      .field("fromUser", "500")
      .field("toUser", "400")
      .field("companyName", "XYZ")
      .field("content", "content");

    expect(res.statusCode).toEqual(403);
  });

  it("should return 404 if any of the users don't exist", async () => {
    const res = await request
      .post("/api/feedback")
      .set("Authorization", "Bearer " + adminToken)
      .field("fromUser", "1")
      .field("toUser", "500")
      .field("companyName", "XYZ")
      .field("content", "content");

    expect(res.statusCode).toEqual(404);
  });

  it("should return 201 if everything is in order", async () => {
    const res = await request
      .post("/api/feedback")
      .set("Authorization", "Bearer " + adminToken)
      .field("fromUser", "1")
      .field("toUser", userId)
      .field("companyName", "XYZ")
      .field("content", "content");

    expect(res.statusCode).toEqual(201);
  });
});

describe("GET /feedback", () => {
  beforeAll(async () => {
    const res = await request
      .post("/api/auth/login")
      .field("email", "test15@test.com")
      .field("password", "testpassword");

    userToken = res.body.token;
  });

  it("should return 403 if a normal user is sending a request", async () => {
    const res = await request
      .get("/api/feedback")
      .set("Authorization", "Bearer " + userToken);

    expect(res.statusCode).toEqual(403);
  });

  it("should return 400 if queries are missing", async () => {
    const res = await request
      .get("/api/feedback")
      .set("Authorization", "Bearer " + adminToken);

    expect(res.statusCode).toEqual(400);
  });

  it("should return 200 if everything is in order", async () => {
    const res = await request
      .get("/api/feedback")
      .set("Authorization", "Bearer " + adminToken)
      .query({
        pageSize: 2,
        page: 1,
      });

    expect(res.statusCode).toEqual(200);
  });
});

describe("GET /:id", () => {
  it("should return 400 if id is invalid", async () => {
    const res = await request.get("/api/feedback/123.3");

    expect(res.statusCode).toEqual(400);
  });

  it("should return 404 if feedback does not exist", async () => {
    const res = await request.get("/api/feedback/500");

    expect(res.statusCode).toEqual(404);
  });

  it("should return 200 if everything is in order", async () => {
    const res = await request.get("/api/feedback/1");

    expect(res.statusCode).toEqual(200);
  });
});

describe("PUT /:id", () => {
  it("should return 400 if id is invalid", async () => {
    const res = await request
      .put("/api/feedback/12.3")
      .set("Authorization", "Bearer " + adminToken);

    expect(res.statusCode).toEqual(400);
  });

  it("should return 404 if feedback does not exist", async () => {
    const res = await request
      .put("/api/feedback/500")
      .set("Authorization", "Bearer " + adminToken)
      .field("fromUser", "1")
      .field("toUser", userId)
      .field("content", "content")
      .field("companyName", "companyName");

    expect(res.statusCode).toEqual(404);
  });

  it("should return 404 if to user cannot be found", async () => {
    const res = await request
      .put("/api/feedback/1")
      .set("Authorization", "Bearer " + adminToken)
      .field("fromUser", "1")
      .field("toUser", "500")
      .field("content", "content")
      .field("companyName", "companyName");

    expect(res.statusCode).toEqual(404);
  });

  it("should return 403 if ids do not match", async () => {
    const res = await request
      .put("/api/feedback/1")
      .set("Authorization", "Bearer " + adminToken)
      .field("fromUser", "400")
      .field("toUser", userId)
      .field("content", "content")
      .field("companyName", "companyName");

    expect(res.statusCode).toEqual(403);
  });

  it("should return 200 if everything is in order", async () => {
    const res = await request
      .put("/api/feedback/1")
      .set("Authorization", "Bearer " + adminToken)
      .field("fromUser", "1")
      .field("toUser", userId)
      .field("content", "content")
      .field("companyName", "companyName");
    expect(res.statusCode).toEqual(200);
  });
});

describe("DELETE /:id", () => {
  let feedbackId: string;
  beforeAll(async () => {
    const res = await request
      .post("/api/feedback")
      .set("Authorization", "Bearer " + userToken)
      .field("fromUser", userId)
      .field("toUser", "1")
      .field("companyName", "XYZ")
      .field("content", "content");

    feedbackId = res.body.id;
  });

  it("should return 400 if id is invalid", async () => {
    const res = await request
      .delete("/api/feedback/12.34")
      .set("Authorization", "Bearer " + adminToken);

    expect(res.statusCode).toEqual(400);
  });

  it("should return 404 if feedback does not exist", async () => {
    const res = await request
      .delete("/api/feedback/500")
      .set("Authorization", "Bearer " + adminToken);

    expect(res.statusCode).toEqual(404);
  });

  it("should return 403 if a user is trying to delete another user's feedback", async () => {
    const res = await request
      .delete("/api/feedback/1")
      .set("Authorization", "Bearer " + userToken);

    expect(res.statusCode).toEqual(403);
  });

  it("should return 204 if an admin is deleting another user's feedback", async () => {
    const res = await request
      .delete("/api/feedback/" + feedbackId)
      .set("Authorization", "Bearer " + adminToken);

    expect(res.statusCode).toEqual(204);
  });

  it("should return 204 if an user is deleting their own feedback", async () => {
    const res = await request
      .delete("/api/feedback/1")
      .set("Authorization", "Bearer " + adminToken);

    expect(res.statusCode).toEqual(204);
  });
});
