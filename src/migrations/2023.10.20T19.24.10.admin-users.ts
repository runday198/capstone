import { Sequelize } from "sequelize";
import { MigrationFn } from "umzug";
import bcrypt from "bcrypt";
import dotenv from "dotenv";
import path from "path";

dotenv.config();

export const up: MigrationFn<Sequelize> = async ({ context }) => {
  const q = context.getQueryInterface();

  const hashPassword = await bcrypt.hash(process.env.ADMIN_PASSWORD, 12);

  await q.bulkInsert("users", [
    {
      first_name: "Admin1",
      last_name: "Admin",
      email: "admin1@admin.com",
      image: path.resolve(__dirname, "../../../public/default.png"),
      title: "Admin",
      summary: "Admin",
      role: "Admin",
      password: hashPassword,
    },
  ]);
};

export const down: MigrationFn<Sequelize> = async ({ context }) => {
  const q = context.getQueryInterface();

  await q.bulkDelete("users", {
    email: "admin1@admin.com",
  });
};
