import { DataTypes, Sequelize } from "sequelize";
import { MigrationFn } from "umzug";

export const up: MigrationFn<Sequelize> = async ({ context }) => {
  const q = context.getQueryInterface();

  await q.createTable("feedbacks", {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true,
    },
    from_user: {
      type: DataTypes.INTEGER.UNSIGNED,
      references: {
        model: {
          tableName: "users",
        },
        key: "id",
      },
      allowNull: false,
      onDelete: "CASCADE",
    },
    to_user: {
      type: DataTypes.INTEGER.UNSIGNED,
      references: {
        model: {
          tableName: "users",
        },
        key: "id",
      },
      allowNull: false,
      onDelete: "CASCADE",
    },
    content: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    company_name: {
      type: new DataTypes.STRING(128),
      allowNull: false,
    },
    created_at: DataTypes.DATE,
    updated_at: DataTypes.DATE,
  });
};
export const down: MigrationFn<Sequelize> = async ({ context }) => {
  const q = context.getQueryInterface();

  await q.dropTable("feedbacks");
};
