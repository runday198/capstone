import { MappedErrors } from "../interfaces/general";
import BaseError from "./BaseError";

class Error400 extends BaseError {
  public errors;

  constructor(
    errors?: MappedErrors | { image?: object; email?: object },
    name = "Error 400",
    statusCode = 400,
    isFatal = false,
    description = "Bad Request"
  ) {
    super(name, statusCode, isFatal, description);
    this.errors = errors;
  }
}

export default Error400;
