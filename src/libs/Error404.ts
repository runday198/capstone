import BaseError from "./BaseError";

class Error404 extends BaseError {
  constructor(
    name = "Error 404",
    statusCode = 404,
    isFatal = false,
    description = "Not Found"
  ) {
    super(name, statusCode, isFatal, description);
  }
}

export default Error404;
