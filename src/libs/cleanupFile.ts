import fs from "fs/promises";

export const cleanupFile = async (filePath: string) => {
  return fs.unlink(filePath);
};
