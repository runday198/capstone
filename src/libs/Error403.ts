import BaseError from "./BaseError";

class Error403 extends BaseError {
  constructor(
    name = "Error 403",
    statusCode = 403,
    isFatal = false,
    description = "Forbidden"
  ) {
    super(name, statusCode, isFatal, description);
  }
}

export default Error403;
