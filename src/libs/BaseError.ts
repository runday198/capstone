class BaseError extends Error {
  public statusCode;
  public isFatal;

  constructor(
    name: string,
    statusCode: number,
    isFatal: boolean,
    description: string
  ) {
    super(description);

    Object.setPrototypeOf(this, new.target.prototype);
    this.name = name;
    this.statusCode = statusCode;
    this.isFatal = isFatal;

    Error.captureStackTrace(this);
  }
}

export default BaseError;
