import pino from "pino-http";
import { logger } from "./logger";

export const requestLogger = pino({
  logger: logger,

  genReqId: function (req) {
    return req.id;
  },
});
