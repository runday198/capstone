import BaseError from "./BaseError";

class Error500 extends BaseError {
  constructor(
    name = "Error 500",
    statusCode = 500,
    isFatal = true,
    description = "Internal Server Error"
  ) {
    super(name, statusCode, isFatal, description);
  }
}

export default Error500;
