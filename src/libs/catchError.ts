import BaseError from "./BaseError";
import Error500 from "./Error500";
import express from "express";

function catchError(
  error: Error,
  next: express.NextFunction
  // req: express.Request
) {
  if (error instanceof BaseError) {
    return next(error);
  }

  return next(new Error500(error.message));
}

export default catchError;
