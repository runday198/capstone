import { Request, Response, NextFunction } from "express";
import BaseError from "./BaseError";
import { ValidationError } from "express-validator";

// eslint-disable-next-line no-unused-vars
export function handleError(
  error: BaseError & { errors: ValidationError[] },
  req: Request,
  res: Response,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  next: NextFunction
) {
  if (error.isFatal || error.statusCode === 500) {
    req.log.error(error);
    return res.status(500).end();
  }

  req.log.info(error);
  return res.status(error.statusCode).json({ errors: error?.errors });
}
