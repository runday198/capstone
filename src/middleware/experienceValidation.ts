import { body } from "express-validator";

export const expValidator = [
  body("companyName", "Please enter company name").trim().isLength({ min: 1 }),
  body("role", "Please enter the role at the company")
    .trim()
    .isLength({ min: 1 }),
  // body("startDate", "Please enter the start date").isDate(),
  // body("endDate")
  //   .optional()
  //   .isDate()
  //   .custom((endDate: Date, { req }) => {
  //     const startDate: Date = req.body.startDate;
  //     if (endDate) {
  //       return startDate < endDate;
  //     }
  //   }),
  body("description", "Please enter description (at least 10 characters)")
    .trim()
    .isLength({ min: 10 }),
];
