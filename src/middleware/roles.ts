import { log } from "console";
import { User, UserRole } from "../models/user.model";
import express from "express";

export const roles = (roles: UserRole[]) => {
  return (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) => {
    const user = req.user as User;

    log(user.role, roles);

    if (!roles.includes(user.role)) {
      return res.status(403).end();
    }

    next();
  };
};
