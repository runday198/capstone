import { body } from "express-validator";

export const registerCheck = [
  body("firstName", "First name can be 1-128 characters long")
    .trim()
    .isLength({ min: 1, max: 128 }),
  body("lastName", "Last name can be 1-128 characters long")
    .trim()
    .isLength({ min: 1, max: 128 }),
  body("title", "Title can be 1-256 characters long")
    .trim()
    .isLength({ min: 1, max: 256 }),
  body("summary", "Summary can be 30-256 characters long")
    .trim()
    .isLength({ min: 3, max: 256 }),
  body("email", "Please enter a valid email adderss")
    .isEmail()
    .normalizeEmail(),
  body("password", "Password must be at least 8 characters long")
    .trim()
    .isLength({ min: 8, max: 255 }),
];

export const registerCheckWithRole = [
  ...registerCheck,
  body("role", "Invalid role").custom((role) => {
    if (role != "Admin" && role != "User") {
      return false;
    }
    return true;
  }),
];
