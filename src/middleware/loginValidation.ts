import { body } from "express-validator";

export const loginCheck = [
  body("email").isEmail().normalizeEmail(),
  body("password", "Invalid email or password.").isLength({ min: 8 }),
];
