import { body } from "express-validator";

export const projectValidator = [
  body("userId").isNumeric(),
  body("description", "Please enter the description").isLength({ min: 1 }),
];
