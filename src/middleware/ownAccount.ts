import { Response, NextFunction } from "express";
import catchError from "../libs/catchError";
// import { User } from "../models/user.model";
import Error403 from "../libs/Error403";
import { cleanupFile } from "../libs/cleanupFile";
import { RequestWithUser } from "../interfaces/express";
import { validationResult } from "express-validator";
import Error400 from "../libs/Error400";

export const ownAccountUser = (adminHasAccess: boolean) => {
  return function (req: RequestWithUser, res: Response, next: NextFunction) {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        throw new Error400(errors.mapped());
      }

      const userId: number = +req.params.id;
      if (adminHasAccess && req.user.role === "Admin") {
        return next();
      }

      if (userId != req.user.id) {
        throw new Error403();
      }

      next();
    } catch (err) {
      if (req.file) {
        cleanupFile(req.file.path);
      }
      catchError(err, next);
    }
  };
};
