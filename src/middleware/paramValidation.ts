import { param } from "express-validator";

export const paramCheck = [
  param("id").trim().isInt({ min: 0 }).withMessage("Invalid ID"),
];

export const userIdCheck = [
  param("userId").trim().isInt({ min: 1 }).withMessage("Invalid ID"),
];
