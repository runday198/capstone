import { body } from "express-validator";

export const feedbackValidator = [
  body("fromUser").isNumeric(),
  body("companyName", "Please provide the company name").isLength({ min: 1 }),
  body("toUser", "Invalid request")
    .isNumeric()
    .custom((id: number, { req }) => {
      if (+id === +req.body.fromUser) {
        return false;
      }
      return true;
    }),
  body("content", "Please provide the context").isLength({ min: 1 }),
];
