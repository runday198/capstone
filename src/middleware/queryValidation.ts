import { query } from "express-validator";

export const queryValidator = [
  query("page")
    .trim()
    .isInt({ min: 1 })
    .withMessage("Page must be a positive integer"),
  query("pageSize")
    .trim()
    .isInt({ min: 1 })
    .withMessage("Page must be a positive integer"),
];
