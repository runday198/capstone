import catchError from "../libs/catchError";
import { RequestWithUser } from "../interfaces/express";
import { RouterFactory } from "../interfaces/general";
import express, { NextFunction, Response } from "express";
import Error400 from "../libs/Error400";
import Error404 from "../libs/Error404";
import { userIdCheck } from "../middleware/paramValidation";
import { validationResult } from "express-validator";

export const makeCVRouter: RouterFactory = (context) => {
  const router = express.Router();

  const userService = context.services.userService;
  const cacheService = context.services.cacheService;

  router.get(
    "/:userId/cv",
    userIdCheck,
    async (req: RequestWithUser, res: Response, next: NextFunction) => {
      try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          throw new Error400(errors.mapped());
        }
        const userId = +req.params.userId;
        const cachedCV = await cacheService.get(userId);
        if (cachedCV) {
          console.log("FROM CACHE");
          return res.status(200).json(JSON.parse(cachedCV));
        }

        const userCv = await userService.getCV(userId);
        if (!userCv) {
          throw new Error404();
        }

        cacheService.set(userId, JSON.stringify(userCv));
        return res.status(200).json(userCv);
      } catch (err) {
        catchError(err, next);
      }
    }
  );

  return router;
};
