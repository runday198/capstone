import { RouterFactory } from "../interfaces/general";
import express, { NextFunction, Request, Response } from "express";
import { registerCheckWithRole } from "../middleware/registerValidation";
import { validationResult } from "express-validator";
import passport from "passport";
import { UserRole } from "../models/user.model";
import { roles } from "../middleware/roles";
import catchError from "../libs/catchError";
import Error400 from "../libs/Error400";
import { cleanupFile } from "../libs/cleanupFile";
import Error404 from "../libs/Error404";
import { ownAccountUser } from "../middleware/ownAccount";
import { RequestWithUser } from "../interfaces/express";
import { queryValidator } from "../middleware/queryValidation";
import { paramCheck } from "../middleware/paramValidation";

export const makeUserRouter: RouterFactory = (context) => {
  const router = express.Router();
  const authService = context.services.authService;
  const userService = context.services.userService;
  const cacheService = context.services.cacheService;

  router.post(
    "/",
    passport.authenticate("jwt", { session: false }),
    roles([UserRole.Admin]),
    registerCheckWithRole,
    async (req: RequestWithUser, res: Response, next: NextFunction) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.mapped() });
      }

      const firstName: string = req.body.firstName;
      const lastName: string = req.body.lastName;
      const title: string = req.body.title;
      const summary: string = req.body.summary;
      const email: string = req.body.email;
      const password: string = req.body.password;
      const role: UserRole = req.body.role;
      const image = req.file;

      try {
        if (!image) {
          throw new Error400({
            image: {
              msg: "Image is required",
              param: "image",
              location: "body",
            },
          });
        }
        if (await authService.emailIsInUse(email)) {
          throw new Error400({
            email: {
              msg: "Email is already in use.",
              param: "email",
              location: "body",
            },
          });
        }

        const hashedPassword = await authService.hashPassword(password);

        const user = await authService.createUser(
          firstName,
          lastName,
          image.path,
          title,
          summary,
          role,
          email,
          hashedPassword
        );

        return res.status(201).json({
          id: user.id,
          firstName: user.firstName,
          lastName: user.lastName,
          title: user.title,
          summary: user.summary,
          email: user.email,
          role: user.role,
        });
      } catch (err) {
        if (req.file) {
          cleanupFile(req.file.path);
        }
        catchError(err, next);
      }
    }
  );

  router.get(
    "/",
    passport.authenticate("jwt", { session: false }),
    roles([UserRole.Admin]),
    queryValidator,
    async (req: RequestWithUser, res: Response, next: NextFunction) => {
      try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          throw new Error400(errors.mapped());
        }
        const pageSize: number = +req.query.pageSize;
        const page: number = +req.query.page;

        const users = await userService.getUsersPage(pageSize, page);

        res.set("X-total-count", String(users.length));
        return res.status(200).json(users);
      } catch (err) {
        catchError(err, next);
      }
    }
  );

  router.get(
    "/:id",
    paramCheck,
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          throw new Error400(errors.mapped());
        }
        const userId: string = req.params.id;

        const user = await userService.findUserById(+userId);
        if (!user) {
          throw new Error404();
        }

        return res.status(200).json({
          id: user.id,
          firstName: user.firstName,
          lastName: user.lastName,
          title: user.title,
          summary: user.summary,
          email: user.email,
          role: user.role,
        });
      } catch (err) {
        catchError(err, next);
      }
    }
  );

  router.put(
    "/:id",
    passport.authenticate("jwt", { session: false }),
    paramCheck,
    ownAccountUser(false),
    registerCheckWithRole,
    async (req: RequestWithUser, res: Response, next: NextFunction) => {
      try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          throw new Error400(errors.mapped());
        }
        const firstName: string = req.body.firstName;
        const lastName: string = req.body.lastName;
        const title: string = req.body.title;
        const summary: string = req.body.summary;
        const email: string = req.body.email;
        const password: string = req.body.password;
        const role: UserRole = req.body.role;
        const avatar = req.file;

        let image = req.user.image;
        const curEmail = req.user.email;
        if (email != curEmail) {
          if (await authService.emailIsInUse(email)) {
            throw new Error400({
              email: {
                msg: "Email is already in use.",
                param: "email",
                location: "body",
              },
            });
          }
        }

        if (avatar) {
          cleanupFile(image);
          image = avatar.path;
        }

        const hashedPassword = await authService.hashPassword(password);

        await userService.updateUser(
          req.user,
          firstName,
          lastName,
          email,
          hashedPassword,
          role,
          title,
          summary,
          image
        );
        await cacheService.clearEntry(req.user.id);

        return res.status(200).json({
          id: req.user.id,
          firstName,
          lastName,
          email,
          role,
          title,
          summary,
        });
      } catch (err) {
        if (req.file) {
          cleanupFile(req.file.path);
        }
        catchError(err, next);
      }
    }
  );

  router.delete(
    "/:id",
    passport.authenticate("jwt", { session: false }),
    paramCheck,
    ownAccountUser(true),
    async (req: RequestWithUser, res: Response, next: NextFunction) => {
      try {
        const id: number = +req.params.id;
        const userToDelete = await userService.findUserById(id);
        if (!userToDelete) {
          throw new Error404();
        }

        await userService.deleteUser(userToDelete);
        await cacheService.clearEntry(userToDelete.id);

        return res.status(204).end();
      } catch (err) {
        catchError(err, next);
      }
    }
  );

  return router;
};
