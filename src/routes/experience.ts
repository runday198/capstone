import { RouterFactory } from "../interfaces/general";
import express, { NextFunction, Request, Response } from "express";
import { RequestWithUser } from "../interfaces/express";
import catchError from "../libs/catchError";
import { expValidator } from "../middleware/experienceValidation";
import { validationResult } from "express-validator";
import Error400 from "../libs/Error400";
import Error404 from "../libs/Error404";
import passport from "passport";
import Error403 from "../libs/Error403";
import { UserRole } from "../models/user.model";
import { roles } from "../middleware/roles";
import { queryValidator } from "../middleware/queryValidation";
import { paramCheck } from "../middleware/paramValidation";

export const makeExperienceRouter: RouterFactory = (context) => {
  const router = express.Router();
  const userService = context.services.userService;
  const expService = context.services.expService;
  const cacheService = context.services.cacheService;

  router.post(
    "/",
    passport.authenticate("jwt", { session: false }),
    expValidator,
    async (req: RequestWithUser, res: Response, next: NextFunction) => {
      try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          throw new Error400(errors.mapped());
        }

        const id: number = +req.body.userId;
        const companyName: string = req.body.companyName;
        const role: string = req.body.role;
        const startDate: Date = req.body.startDate;
        const endDate: Date = req.body.endDate;
        const description: string = req.body.description;

        if (id !== req.user.id) {
          throw new Error403();
        }
        if (!(await userService.findUserById(id))) {
          throw new Error404();
        }

        const experience = await expService.createExperience(
          id,
          companyName,
          role,
          startDate,
          endDate,
          description
        );
        await cacheService.clearEntry(id);

        return res.status(201).json({
          id: experience.id,
          userId: experience.userId,
          companyName: experience.companyName,
          role: experience.role,
          startDate: experience.startDate,
          endDate: experience.endDate,
          description: experience.description,
        });
      } catch (err) {
        catchError(err, next);
      }
    }
  );

  router.get(
    "/",
    passport.authenticate("jwt", { session: false }),
    roles([UserRole.Admin]),
    queryValidator,
    async (req: RequestWithUser, res: Response, next: NextFunction) => {
      try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          throw new Error400(errors.mapped());
        }
        const pageSize: number = +req.query.pageSize;
        const page: number = +req.query.page;

        const experiences = await expService.getExperiencesPage(pageSize, page);

        res.set("X-total-count", String(experiences.length));
        return res.status(200).json(experiences);
      } catch (err) {
        catchError(err, next);
      }
    }
  );

  router.get(
    "/:id",
    paramCheck,
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          throw new Error400(errors.mapped());
        }
        const id: number = +req.params.id;

        const experience = await expService.findExperienceById(id);
        if (!experience) {
          throw new Error404();
        }

        return res.status(200).json({
          id: experience.id,
          userId: experience.userId,
          companyName: experience.companyName,
          role: experience.role,
          startDate: experience.startDate,
          endDate: experience.endDate,
          description: experience.description,
        });
      } catch (err) {
        catchError(err, next);
      }
    }
  );

  router.put(
    "/:id",
    passport.authenticate("jwt", { session: false }),
    expValidator,
    paramCheck,
    async (req: RequestWithUser, res: Response, next: NextFunction) => {
      try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          throw new Error400(errors.mapped());
        }
        const expId = +req.params.id;

        const userId: number = +req.body.userId;
        const companyName: string = req.body.companyName;
        const role: string = req.body.role;
        const startDate: Date = req.body.startDate;
        const endDate: Date = req.body.endDate;
        const description: string = req.body.description;

        const experience = await expService.findExperienceById(expId);
        if (!experience) {
          throw new Error404();
        }
        if (experience.userId !== userId) {
          throw new Error400();
        }
        if (req.user.role === "User" && experience.userId !== req.user.id) {
          throw new Error403();
        }

        const updatedExperience = await expService.updateExperience(
          experience,
          userId,
          companyName,
          role,
          startDate,
          endDate,
          description
        );

        await cacheService.clearEntry(experience.userId);

        return res.status(200).json({
          id: expId,
          userId: updatedExperience.userId,
          companyName: updatedExperience.companyName,
          role: updatedExperience.role,
          startDate: updatedExperience.startDate,
          endDate: updatedExperience.endDate,
          description: updatedExperience.description,
        });
      } catch (err) {
        catchError(err, next);
      }
    }
  );

  router.delete(
    "/:id",
    passport.authenticate("jwt", { session: false }),
    paramCheck,
    async (req: RequestWithUser, res: Response, next: NextFunction) => {
      try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          throw new Error400(errors.mapped());
        }
        const expId: number = +req.params.id;

        const experience = await expService.findExperienceById(expId);
        if (!experience) {
          throw new Error404();
        }
        if (experience.userId !== req.user.id && req.user.role !== "Admin") {
          throw new Error403();
        }

        await cacheService.clearEntry(experience.userId);
        await expService.deleteExperience(experience);
        return res.status(204).end();
      } catch (err) {
        catchError(err, next);
      }
    }
  );

  return router;
};
