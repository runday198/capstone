import { Context, RouterFactory } from "../interfaces/general";
import express from "express";
import { registerCheck } from "../middleware/registerValidation";
import { validationResult } from "express-validator";
import { User, UserRole } from "../models/user.model";
import passport from "passport";
import Error400 from "../libs/Error400";
import catchError from "../libs/catchError";
import { cleanupFile } from "../libs/cleanupFile";

export const makeAuthRouter: RouterFactory = (context: Context) => {
  const router = express.Router();

  // Define routes

  router.post(
    "/register",
    registerCheck,
    async (
      req: express.Request,
      res: express.Response,
      next: express.NextFunction
    ) => {
      try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          throw new Error400(errors.mapped());
        }
        const email: string = req.body.email;
        const firstName: string = req.body.firstName;
        const lastName: string = req.body.lastName;
        const title: string = req.body.title;
        const summary: string = req.body.summary;
        const password: string = req.body.password;
        const image = req.file;

        if (!image) {
          throw new Error400({
            image: {
              msg: "Image is required",
              param: "avatar",
              location: "body",
            },
          });
        }

        if (await context.services.authService.emailIsInUse(email)) {
          throw new Error400({
            email: {
              msg: "Email is already in use.",
              param: "email",
              location: "body",
            },
          });
        }

        const hashedPassword =
          await context.services.authService.hashPassword(password);

        const user = await context.services.authService.createUser(
          firstName,
          lastName,
          image.path,
          title,
          summary,
          UserRole.User,
          email,
          hashedPassword
        );

        return res.status(201).json({
          id: user.id,
          firstName: user.firstName,
          lastName: user.lastName,
          title: user.title,
          summary: user.summary,
          email: user.email,
          image: user.image,
        });
      } catch (err) {
        if (req.file) {
          cleanupFile(req.file.path);
        }
        catchError(err, next);
      }
    }
  );

  router.post(
    "/login",
    passport.authenticate("local", { session: false }),
    async (req: express.Request, res: express.Response) => {
      const user: User = req.user as User;

      const token = context.services.authService.generateJWT(
        user.email,
        user.id
      );

      return res.status(200).json({
        user: {
          id: user.id,
          firstName: user.firstName,
          lastName: user.lastName,
          title: user.lastName,
          summary: user.summary,
          email: user.email,
          image: user.image,
        },
        token,
      });
    }
  );

  return router;
};
