import { RouterFactory } from "../interfaces/general";
import express, { NextFunction, Request, Response } from "express";
import passport from "passport";
import { RequestWithUser } from "../interfaces/express";
import { projectValidator } from "../middleware/projectValidation";
import { validationResult } from "express-validator";
import catchError from "../libs/catchError";
import Error400 from "../libs/Error400";
import Error403 from "../libs/Error403";
import Error404 from "../libs/Error404";
import { UserRole } from "../models/user.model";
import { roles } from "../middleware/roles";
import { cleanupFile } from "../libs/cleanupFile";
import { queryValidator } from "../middleware/queryValidation";
import { paramCheck } from "../middleware/paramValidation";

export const makeProjectsRouter: RouterFactory = (context) => {
  const router = express.Router();
  const projectService = context.services.projectService;
  const cacheService = context.services.cacheService;

  router.post(
    "/",
    passport.authenticate("jwt", { session: false }),
    projectValidator,
    async (req: RequestWithUser, res: Response, next: NextFunction) => {
      try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          throw new Error400(errors.mapped());
        }

        const userId: number = +req.body.userId;
        if (req.user.id !== userId) {
          throw new Error403();
        }
        const description: string = req.body.description;
        const image = req.file;
        if (!image) {
          throw new Error400({
            image: {
              msg: "Image is required",
              param: "avatar",
              location: "body",
            },
          });
        }

        const project = await projectService.createProject(
          userId,
          image.path,
          description
        );

        await cacheService.clearEntry(userId);

        return res.status(201).json({
          id: project.id,
          userId: project.userId,
          image: project.image,
          description: project.description,
        });
      } catch (err) {
        if (req.file) {
          cleanupFile(req.file.path);
        }
        catchError(err, next);
      }
    }
  );

  router.get(
    "/",
    passport.authenticate("jwt", { session: false }),
    queryValidator,
    roles([UserRole.Admin]),
    async (req: RequestWithUser, res: Response, next: NextFunction) => {
      try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          throw new Error400(errors.mapped());
        }
        const pageSize: number = +req.query.pageSize;
        const page: number = +req.query.page;

        const projects = await projectService.getProjectsPage(pageSize, page);

        res.set("X-total-count", String(projects.length));
        return res.status(200).json(projects);
      } catch (err) {
        catchError(err, next);
      }
    }
  );

  router.get(
    "/:id",
    paramCheck,
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          throw new Error400(errors.mapped());
        }
        const projectId = +req.params.id;

        const project = await projectService.findProjectById(projectId);
        if (!project) {
          throw new Error404();
        }

        return res.status(200).json({
          id: project.id,
          userId: project.userId,
          image: project.image,
          description: project.description,
        });
      } catch (err) {
        catchError(err, next);
      }
    }
  );

  router.put(
    "/:id",
    passport.authenticate("jwt", { session: false }),
    projectValidator,
    paramCheck,
    async (req: RequestWithUser, res: Response, next: NextFunction) => {
      try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          throw new Error400(errors.mapped());
        }

        const projectId = +req.params.id;

        const userId: number = +req.body.userId;
        const description: string = req.body.description;
        const image = req.file;
        if (req.user.id !== userId && req.user.role !== "Admin") {
          throw new Error403();
        }

        const project = await projectService.findProjectById(projectId);
        if (!project) {
          throw new Error404();
        }
        if (project.userId !== userId) {
          throw new Error403();
        }
        let curImage = project.image;
        if (image) {
          cleanupFile(project.image);
          curImage = image.path;
        }

        const updatedProject = await projectService.updateProject(
          project,
          description,
          curImage
        );

        await cacheService.clearEntry(project.userId);

        return res.status(200).json({
          id: updatedProject.id,
          userId: updatedProject.userId,
          image: updatedProject.image,
          description: updatedProject.description,
        });
      } catch (err) {
        if (req.file) {
          cleanupFile(req.file.path);
        }
        catchError(err, next);
      }
    }
  );

  router.delete(
    "/:id",
    passport.authenticate("jwt", { session: false }),
    paramCheck,
    async (req: RequestWithUser, res: Response, next: NextFunction) => {
      try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          throw new Error400(errors.mapped());
        }
        const projectId = +req.params.id;

        const project = await projectService.findProjectById(projectId);
        if (!project) {
          throw new Error404();
        }

        if (project.userId !== req.user.id && req.user.role !== "Admin") {
          throw new Error403();
        }

        await projectService.deleteProject(project);
        await cacheService.clearEntry(project.userId);

        return res.status(204).end();
      } catch (err) {
        catchError(err, next);
      }
    }
  );

  return router;
};
