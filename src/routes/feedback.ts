import passport from "passport";
import { RouterFactory } from "../interfaces/general";
import express, { NextFunction, Request, Response } from "express";
import { feedbackValidator } from "../middleware/feedbackValidation";
import { RequestWithUser } from "../interfaces/express";
import { validationResult } from "express-validator";
import catchError from "../libs/catchError";
import Error400 from "../libs/Error400";
import Error404 from "../libs/Error404";
import Error403 from "../libs/Error403";
import { roles } from "../middleware/roles";
import { UserRole } from "../models/user.model";
import { queryValidator } from "../middleware/queryValidation";
import { paramCheck } from "../middleware/paramValidation";

export const makeFeedbackRouter: RouterFactory = (context) => {
  const router = express.Router();
  const feedService = context.services.feedService;
  const userService = context.services.userService;
  const cacheService = context.services.cacheService;

  router.post(
    "/",
    passport.authenticate("jwt", { session: false }),
    feedbackValidator,
    async (req: RequestWithUser, res: Response, next: NextFunction) => {
      try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          throw new Error400(errors.mapped());
        }

        const fromUser: number = +req.body.fromUser;
        if (req.user.id !== fromUser) {
          throw new Error403();
        }
        const toUser: number = +req.body.toUser;
        const companyName: string = req.body.companyName;
        const content: string = req.body.content;
        if (
          !(await userService.findUserById(fromUser)) ||
          !(await userService.findUserById(toUser))
        ) {
          throw new Error404();
        }

        const feedback = await feedService.createFeedback(
          fromUser,
          toUser,
          content,
          companyName
        );

        await cacheService.clearEntry(toUser);
        return res.status(201).json({
          fromUser: feedback.fromUser,
          toUser: feedback.toUser,
          content: feedback.content,
          companyName: feedback.companyName,
          id: feedback.id,
        });
      } catch (err) {
        catchError(err, next);
      }
    }
  );

  router.get(
    "/",
    passport.authenticate("jwt", { session: false }),
    roles([UserRole.Admin]),
    queryValidator,
    async (req: RequestWithUser, res: Response, next: NextFunction) => {
      try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          throw new Error400(errors.mapped());
        }
        const pageSize: number = +req.query.pageSize;
        const page: number = +req.query.page;

        const feedbacks = await feedService.getFeedbacksPage(pageSize, page);

        res.set("X-total-count", String(feedbacks.length));
        return res.status(200).json(feedbacks);
      } catch (err) {
        catchError(err, next);
      }
    }
  );

  router.get(
    "/:id",
    paramCheck,
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          throw new Error400(errors.mapped());
        }
        const feedbackId = +req.params.id;
        const feedback = await feedService.findFeedbackById(feedbackId);
        if (!feedback) {
          throw new Error404();
        }

        return res.status(200).json({
          id: feedback.id,
          companyName: feedback.companyName,
          content: feedback.content,
          fromUser: feedback.fromUser,
          toUser: feedback.toUser,
        });
      } catch (err) {
        catchError(err, next);
      }
    }
  );

  router.put(
    "/:id",
    passport.authenticate("jwt", { session: false }),
    feedbackValidator,
    paramCheck,
    async (req: RequestWithUser, res: Response, next: NextFunction) => {
      try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          throw new Error400(errors.mapped());
        }
        const feedId = +req.params.id;

        const fromUser: number = +req.body.fromUser;
        const companyName: string = req.body.companyName;
        const content: string = req.body.content;
        const toUser: number = +req.body.toUser;

        const feedback = await feedService.findFeedbackById(feedId);
        if (!feedback) {
          throw new Error404();
        }
        if (!(await userService.findUserById(toUser))) {
          throw new Error404();
        }
        if (
          (req.user.id !== feedback.fromUser && req.user.role !== "Admin") ||
          feedback.fromUser !== fromUser
        ) {
          throw new Error403();
        }

        const oldToUser = feedback.toUser;
        const updatedFeedback = await feedService.updateFeedback(
          feedback,
          fromUser,
          toUser,
          content,
          companyName
        );

        // CLEARING CACHE FOR PREVIOUS TOUSER AND NEW TOUSER
        await cacheService.clearEntry(toUser);
        await cacheService.clearEntry(oldToUser);

        return res.status(200).json({
          id: updatedFeedback.id,
          fromUser: updatedFeedback.fromUser,
          toUser: updatedFeedback.toUser,
          companyName: updatedFeedback.companyName,
          content: updatedFeedback.content,
        });
      } catch (err) {
        catchError(err, next);
      }
    }
  );

  router.delete(
    "/:id",
    passport.authenticate("jwt", { session: false }),
    paramCheck,
    async (req: RequestWithUser, res: Response, next: NextFunction) => {
      try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          throw new Error400(errors.mapped());
        }
        const feedId = +req.params.id;

        const feedback = await feedService.findFeedbackById(feedId);
        if (!feedback) {
          throw new Error404();
        }
        if (req.user.id !== feedback.fromUser && req.user.role !== "Admin") {
          throw new Error403();
        }

        await feedService.deleteFeedback(feedback);
        await cacheService.clearEntry(feedback.toUser);

        return res.status(204).end();
      } catch (err) {
        catchError(err, next);
      }
    }
  );

  return router;
};
