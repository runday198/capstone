import { Loader } from "../interfaces/general";
import requestId from "express-request-id";
import { logger } from "../libs/logger";
import { ExtendedRequest } from "../interfaces/express";
import { requestLogger } from "../libs/request.logger";
import express from "express";
import multer from "multer";
import { filter, storage } from "../libs/multer";

export const loadMiddlewares: Loader = (app) => {
  app.use(requestId());

  app.use((req: ExtendedRequest, res, next) => {
    const requestId = req.id;
    req.log = logger.child({ requestId });
    next();
  });

  app.use(requestLogger);

  app.use(express.json());
  app.use(multer({ storage, fileFilter: filter }).single("image"));

  process
    .on("uncaughtException", (err) => {
      logger.error(err);
      process.exit(1);
    })
    .on("unhandledRejection", (reason) => {
      logger.error(reason);
    });
};
