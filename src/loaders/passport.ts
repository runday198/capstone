import { Context, Loader } from "../interfaces/general";
import { Strategy as LocalStrategy } from "passport-local";
import { Strategy as JwtStrategy, ExtractJwt } from "passport-jwt";
import passport from "passport";
import dotenv from "dotenv";

dotenv.config();

export const loadPassport: Loader = (app, context: Context) => {
  const { authService, userService } = context.services;

  const customFields = {
    usernameField: "email",
    passwordField: "password",
  };

  const localStrategy = new LocalStrategy(customFields, async function (
    email,
    password,
    done
  ) {
    try {
      const user = await userService.findUserByEmail(email);
      if (!user) {
        return done(null, false);
      }

      if (!(await authService.comparePasswords(password, user.password))) {
        return done(null, false);
      }

      return done(null, user);
    } catch (err) {
      done(err);
    }
  });

  const options = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: process.env.JWT_KEY,
  };

  const jwtStrategy = new JwtStrategy(options, async function (
    jwtPayload: { id: number; email: string },
    done
  ) {
    try {
      const user = await userService.findUserById(jwtPayload.id);

      if (!user) {
        return done(null, false);
      }

      return done(null, user);
    } catch (err) {
      done(err);
    }
  });

  app.use(passport.initialize());
  passport.use(localStrategy);
  passport.use(jwtStrategy);
};
