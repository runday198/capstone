import { UserService } from "../services/user.service";
import { Context } from "../interfaces/general";
import { AuthService } from "../services/auth.service";
import { ExperienceService } from "../services/experience.service";
import { FeedbackService } from "../services/feedback.service";
import { ProjectService } from "../services/project.service";
import { cacheService } from "../services/cache.service";
// import { Config } from "../config";

export const loadContext = async (): Promise<Context> => {
  await cacheService.connect();

  return {
    services: {
      authService: new AuthService(),
      userService: new UserService(),
      expService: new ExperienceService(),
      feedService: new FeedbackService(),
      projectService: new ProjectService(),
      cacheService: cacheService,
    },
  };
};
