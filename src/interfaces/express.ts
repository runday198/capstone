import { Request } from "express";
import { P } from "pino";
import { User } from "../models/user.model";

export type ExtendedRequest = Request & { id: string; log: P.Logger };

export type RequestWithUser = Request & { user: User };
