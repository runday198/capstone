import express from "express";
import { AuthService } from "../services/auth.service";
import { User } from "../models/user.model";
import { Project } from "../models/project.model";
import { Experience } from "../models/experience.model";
import { UserService } from "../services/user.service";
import { ValidationError } from "express-validator";
import { ExperienceService } from "../services/experience.service";
import { FeedbackService } from "../services/feedback.service";
import { Feedback } from "../models/feedback.model";
import { ProjectService } from "../services/project.service";
import { CacheService } from "../services/cache.service";

export interface Context {
  services: {
    authService: AuthService;
    userService: UserService;
    expService: ExperienceService;
    feedService: FeedbackService;
    projectService: ProjectService;
    cacheService: CacheService;
  };
}

export type RouterFactory = (context: Context) => express.Router;

export type Loader = (app: express.Application, context: Context) => void;

export interface Models {
  user: typeof User;
  project: typeof Project;
  feedback: typeof Feedback;
  experience: typeof Experience;
}

export type MappedErrors = Record<string, ValidationError>;
