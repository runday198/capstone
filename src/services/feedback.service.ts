import { Feedback } from "../models/feedback.model";

export class FeedbackService {
  async createFeedback(
    fromUser: number,
    toUser: number,
    content: string,
    companyName: string
  ) {
    return Feedback.create({
      fromUser,
      toUser,
      companyName,
      content,
    });
  }

  async getFeedbacksPage(pageSize: number, page: number) {
    return Feedback.findAll({ limit: pageSize, offset: (page - 1) * pageSize });
  }

  async findFeedbackById(id: number) {
    return Feedback.findByPk(id);
  }

  async updateFeedback(
    feedback: Feedback,
    fromUser: number,
    toUser: number,
    content: string,
    companyName: string
  ) {
    feedback.fromUser = fromUser;
    feedback.toUser = toUser;
    feedback.content = content;
    feedback.companyName = companyName;

    return feedback.save();
  }

  async deleteFeedback(feedback: Feedback) {
    return feedback.destroy();
  }
}
