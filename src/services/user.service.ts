import { Project } from "../models/project.model";
import { cleanupFile } from "../libs/cleanupFile";
import { User, UserRole } from "../models/user.model";
import { Experience } from "../models/experience.model";
import { Feedback } from "../models/feedback.model";

export class UserService {
  async findUserByEmail(email: string) {
    return User.findOne({ where: { email } });
  }

  async findUserById(id: number) {
    return User.findByPk(id);
  }

  async getUsersPage(pageSize: number, page: number): Promise<User[]> {
    return User.findAll({
      limit: pageSize,
      offset: (page - 1) * pageSize,
      attributes: { exclude: ["password"] },
    });
  }

  async updateUser(
    user: User,
    firstName: string,
    lastName: string,
    email: string,
    password: string,
    role: UserRole,
    title: string,
    summary: string,
    image: string
  ): Promise<User> {
    user.firstName = firstName;
    user.lastName = lastName;
    user.email = email;
    user.password = password;
    user.role = role;
    user.title = title;
    user.summary = summary;
    user.image = image;

    return user.save();
  }

  async deleteUser(user: User) {
    await cleanupFile(user.image);
    await user.destroy();
  }

  async getCV(userId: number) {
    return User.findByPk(userId, {
      include: [
        { model: Project, required: false },
        { model: Experience, required: false },
        { model: Feedback, where: { toUser: userId }, required: false },
      ],
      attributes: { exclude: ["password"] },
    });
  }
}
