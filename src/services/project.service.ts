import { cleanupFile } from "../libs/cleanupFile";
import { Project } from "../models/project.model";

export class ProjectService {
  async createProject(userId: number, image: string, description: string) {
    return Project.create({ userId, description, image });
  }

  async getProjectsPage(pageSize: number, page: number) {
    return Project.findAll({ limit: pageSize, offset: (page - 1) * pageSize });
  }

  async findProjectById(id: number) {
    return Project.findByPk(id);
  }

  async updateProject(project: Project, description: string, image: string) {
    project.description = description;
    project.image = image;

    return project.save();
  }

  async deleteProject(project: Project) {
    cleanupFile(project.image);
    return project.destroy();
  }
}
