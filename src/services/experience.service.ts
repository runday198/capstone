import { Experience } from "../models/experience.model";

export class ExperienceService {
  async createExperience(
    userId: number,
    companyName: string,
    role: string,
    startDate: Date,
    endDate: Date,
    description: string
  ) {
    return Experience.create({
      userId,
      companyName,
      role,
      startDate,
      endDate,
      description,
    });
  }

  async getExperiencesPage(pageSize: number, page: number) {
    return Experience.findAll({
      limit: pageSize,
      offset: (page - 1) * pageSize,
    });
  }

  async findExperienceById(id: number) {
    return Experience.findByPk(id);
  }

  async updateExperience(
    experience: Experience,
    userId: number,
    companyName: string,
    role: string,
    startDate: Date,
    endDate: Date,
    description: string
  ) {
    experience.userId = userId;
    experience.companyName = companyName;
    experience.role = role;
    experience.startDate = startDate;
    experience.endDate = endDate;
    experience.description = description;
    return experience.save();
  }

  async deleteExperience(experience: Experience) {
    return experience.destroy();
  }
}
