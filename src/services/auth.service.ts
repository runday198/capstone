import { User, UserRole } from "../models/user.model";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import dotenv from "dotenv";

dotenv.config();

export class AuthService {
  async emailIsInUse(email: string): Promise<boolean> {
    const user = await User.findOne({ where: { email } });
    if (user) {
      return true;
    }
    return false;
  }

  async hashPassword(password: string): Promise<string> {
    return bcrypt.hash(password, 12);
  }

  async comparePasswords(
    password: string,
    encrypted: string
  ): Promise<boolean> {
    return bcrypt.compare(password, encrypted);
  }

  async createUser(
    firstName: string,
    lastName: string,
    image: string,
    title: string,
    summary: string,
    role: UserRole,
    email: string,
    password: string
  ) {
    return User.create({
      firstName,
      lastName,
      image,
      title,
      summary,
      role,
      email,
      password,
    });
  }

  generateJWT(email: string, id: number) {
    return jwt.sign({ id, email }, process.env.JWT_KEY);
  }
}
