import { createClient } from "redis";

const env = process.env.NODE_ENV as "development" | "test";
const redisPort = env === "development" ? 6379 : 6380;

export class CacheService {
  public client: ReturnType<typeof createClient>;

  constructor() {
    this.client = createClient({
      url: `redis://localhost:${redisPort}`,
    });
    this.client.on("error", (err) => console.log(`REDIS ERROR: `, err));
  }

  async connect() {
    await this.client.connect();
  }

  async set(id: number, key: string) {
    await this.client.set(`${id}`, key);
  }

  async get(id: number) {
    return this.client.get(`${id}`);
  }

  async clearEntry(id: number) {
    this.client.del(`${id}`);
  }

  async disconnect() {
    return this.client.disconnect();
  }

  async quit() {
    return this.client.quit();
  }
}

export const cacheService = new CacheService();
