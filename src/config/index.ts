export interface Config {
  db: {
    host: string;
    port: number;
    username: string;
    password: string;
    database: string;
  };
  redis: {
    host: string;
    port: number;
  };
  auth: {
    secret: string;
  };
}

const configs: {
  development: Config;
  test: Config;
} = {
  development: {
    db: {
      host: "localhost",
      port: 3306,
      username: "dev",
      password: "dev",
      database: "capstone_project",
    },
    redis: {
      host: "localhost",
      port: 6379,
    },
    auth: {
      secret: "some-dev-secret",
    },
  },
  test: {
    db: {
      host: "localhost",
      port: 3307,
      username: "test",
      password: "test",
      database: "capstone_project_test",
    },
    redis: {
      host: "localhost",
      port: 6380,
    },
    auth: {
      secret: "some-dev-secret",
    },
  },
};

const getConfig = (): Config => {
  if (!process.env.NODE_ENV) {
    throw new Error(
      'Env parameter NODE_ENV must be specified! Possible values are "development", "test", ...'
    );
  }

  const env = process.env.NODE_ENV as "development" | "test";

  console.log(process.env.NODE_ENV);

  if (!configs[env]) {
    throw new Error(
      'Unsupported NODE_ENV value was provided! Possible values are "development", "test", ...'
    );
  }

  return configs[env];
};

export const config = getConfig();
